<?php 
error_reporting(E_ALL ^ E_NOTICE); // hide all basic notices from PHP

//If the form is submitted
if(isset($_POST['submitted'])) {
  
  // require a first name from user
  if(trim($_POST['firstname']) === '') {
    $nameError =  'Forgot your first name!'; 
    $hasError = true;
  } else {
    $firstname = trim($_POST['firstname']);
  }
  
  // require a last name from user
  if(trim($_POST['lastname']) === '') {
    $nameError =  'Forgot your last name!'; 
    $hasError = true;
  } else {
    $lastname = trim($_POST['lastname']);
  }
  
  // need phone number
  if(trim($_POST['phone']) === '') {
    $nameError =  'Forgot to enter in your phone number.'; 
    $hasError = true;
  } else {
    $phone = trim($_POST['phone']);
  }
  
  // need valid email
  if(trim($_POST['email']) === '')  {
    $emailError = 'Forgot to enter in your e-mail address.';
    $hasError = true;
  } else if (!preg_match("/^[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/i", trim($_POST['email']))) {
    $emailError = 'You entered an invalid email address.';
    $hasError = true;
  } else {
    $email = trim($_POST['email']);
  }
  
  // need a times
  if(trim($_POST['times']) === '') {
    $nameError =  'Forgot to choose a time most available.'; 
    $hasError = true;
  } else {
    $times = trim($_POST['times']);
  }

  // comments
  $comments = trim($_POST['comments']);
    
  // upon no failure errors let's email now!
  if(!isset($hasError)) {
    
    $emailTo = 'evan@jameswoodsforcongress.com, tyler@tylerwalters.com';
    $subject = 'James Woods For Congress Volunteer: '. $firstname . ' ' . $lastname;
    $sendCopy = trim($_POST['sendCopy']);
    $body = "First Name: $firstname \n\nLast Name: $lastname \n\nPhone: $phone \n\nEmail: $email \n\nAddress: $address \n\nCity: $city \n\nBest Time Available: $times \n\nZip: $zip";
    $headers = 'From: ' .' <'.$emailTo.'>' . "\r\n" . 'Reply-To: ' . $email;

    mail($emailTo, $subject, $body, $headers);
        
        // set our boolean completion value to TRUE
    $emailSent = true;
  }
}
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <title>Volunteer - James Woods For Congress</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="shortcut icon" href="/favicon.ico">
    <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,700' rel='stylesheet' type='text/css'>

    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link rel="stylesheet" href="styles/main.css">

  </head>
  <body>

    <div class="container-fluid">

      <div id="header" class="row header">
        <div class="container">
          <a href="index.html"><img src="images/logo.png" class="logo" alt="Woods For Congress" /></a>
          <span class="tagline pull-right"><img src="images/tagline.png" alt="help America regain its vision" /></span>
        </div>
      </div>

      <nav class="navbar navbar-default" role="navigation">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <a class="navbar-brand visible-ex-sm visible-xs" href="#"><img src="images/logo.png" alt="Woods For Congress" /></a>
          </div>
        </div><!-- /.container-fluid -->
      </nav>

      <div class="row content">
        <div class="container">

          <div class="col-sm-12 volunteer">

            <section id="signup-form">
              <?php if(isset($emailSent) && $emailSent == true) { ?>
              <h3>Thank you!</h3>
              <p>Thanks for submitting your information! We will be in touch with you soon.</p>
              <?php } else { ?>

              <?php if(isset($hasError) || isset($captchaError) ) { ?>
              <p class="alert">Error submitting the form</p>
              <?php } ?>

              <h3 class='title'>Volunteer</h3>
              
              <form id="signup" action="volunteer.php" method="post">
                <div class="form-group">
                  <label for="firstname">First Name</label>
                  <input name="firstname" id="firstname" class="form-control" placeholder="First Name" type="text" value="<?php if(isset($_POST['firstname'])) echo $_POST['firstname'];?>" tabindex="1" required autofocus>
                  <?php if($nameError != '') { ?>  
                  <br /><span class="error"><?php echo $firstnameError;?></span>   
                  <?php } ?>
                </div>
                
                <div class="form-group">
                  <label for="lastname">Last Name</label>
                  <input name="lastname" id="lastname" class="form-control" placeholder="Last Name" type="text" value="<?php if(isset($_POST['lastname'])) echo $_POST['lastname'];?>" tabindex="2" required>
                  <?php if($nameError != '') { ?>  
                  <br /><span class="error"><?php echo $lastnameError;?></span>   
                  <?php } ?>
                </div>

                <div class="form-group">
                  <label for="phone">Phone Number</label>
                  <input name="phone" id="phone" class="form-control" placeholder="Eg. (888) 867-5309" type="tel" value="<?php if(isset($_POST['phone'])) echo $_POST['phone'];?>" tabindex="3" required>
                  <?php if($nameError != '') { ?>  
                  <br /><span class="error"><?php echo $phoneError;?></span>   
                  <?php } ?>
                </div>

                <div class="form-group">
                  <label for="emailaddress">Email Address</label>
                  <input name="email" id="emailaddress" class="form-control" placeholder="Eg. example@yourdomain.com" type="email" value="<?php if(isset($_POST['email'])) echo $_POST['email'];?>" tabindex="4" required>
                  <?php if($emailError != '') { ?>
                  <br /><span class="error"><?php echo $emailError;?></span>
                  <?php } ?>
                </div>
                 
                <div class="form-group">
                  <label for="timesavailable">Time You Are Most Available</label>
                  <select name="times" id="timesavailable" class="form-control" tabindex="5" required>
                  <option value="">Select One</option>
                  <option value="morning">Morning</option>
                  <option value="afternoon">Afternoon</option>
                  <option value="evening">Evening</option>
                  </optgroup>
                  </select>
                  <?php if($nameError != '') { ?>  
                  <br /><span class="error"><?php echo $timesError;?></span>   
                  <?php } ?>
                </div> 

                <div class="form-group">
                  <label for="comments">Additional Comments</label>
                  <textarea name="comments"id="comments" class="form-control" value="<?php if(isset($_POST['comments'])) echo $_POST['comments'];?>" cols="30" rows="10" tabindex="6"></textarea>
                </div>
                 

                <button name="submit" class="btn btn-primary" type="submit" id="contact-submit">Submit!</button>
                <input type="hidden" name="submitted" id="submitted" value="true" />
              </form>
              
            <?php } ?>
            </section>
          </div>

        </div>
      </div>

      <div class="row footer">
        <div class="container">
          <a href="index.html"><img src="images/logo.png" class="logo" alt="Woods For Congress" /></a>
          <span class="tagline pull-right"><img src="images/tagline.png" alt="help America regain its vision" /></span>
        </div>
      </div>

      <div class="paidFor">
        <p>Authorized and paid for by the James Woods for Congress Campaign Committee.</p>
      </div>

    </div>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-48509909-1');ga('send','pageview');
    </script>

    <script src="scripts/main.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>

    <script type="text/javascript">
      <!--//--><![CDATA[//><!--
      $(document).ready(function() {
        $('form#signup').submit(function() {
          $('form#signup .error').remove();
          var hasError = false;
          $('.requiredField').each(function() {
            if($.trim($(this).val()) == '') {
              var labelText = $(this).prev('label').text();
              $(this).parent().append('<span class="error">Your forgot to enter your '+labelText+'.</span>');
              $(this).addClass('inputError');
              hasError = true;
            } else if($(this).hasClass('email')) {
              var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
              if(!emailReg.test($.trim($(this).val()))) {
                var labelText = $(this).prev('label').text();
                $(this).parent().append('<span class="error">Sorry! You\'ve entered an invalid '+labelText+'.</span>');
                $(this).addClass('inputError');
                hasError = true;
              }
            }
          });
          if(!hasError) {
            var formInput = $(this).serialize();
            $.post($(this).attr('action'),formInput, function(data){
              $('form#signup').slideUp("fast", function() {          
                $(this).before('<h3>Thank you!</h3><p>Thanks for submitting your information! We will be in touch with you soon.</p>');
                $('.title').hide();
              });
            });
          }
          
          return false; 
        });
      });
      //-->!]]>
    </script>
</body>
</html>
