'use strict';

$(document).ready(function () {
  var origOffsetY = 125;

  $(window).scroll(function () {
    // Changes navbar to fixed location at top if scrolled down sufficiently.
    if ($(window).scrollTop() >= origOffsetY || $(window).width() < 768) {
      $('.navbar').addClass('navbar-fixed-top').css( 'border-bottom', '2px solid #e7e7e7');
    } else {
      $('.navbar').removeClass('navbar-fixed-top').css( 'border-bottom', '2px solid #005790');
    }

    // Adds active class to active nav item's li.
    var y = $(this).scrollTop();

    $('.link').each(function () {
      if (y >= $($(this).attr('href')).offset().top - 120) {
        $('.link').not(this).parent( 'li' ).removeClass('active');
        $(this).parent( 'li' ).addClass('active');
      }
    });
  });

  // Animate scroll to section from navigation
  $(".link").click(function() {
    event.preventDefault();
    var target = $(this).attr("href");
    $('html, body').animate({
      scrollTop: $(window).scrollTop() >= origOffsetY ? ($(target).offset().top) - 50 : ($(target).offset().top) - 100
    }, 2000);
  });

  // Platforms/Bio slideshow
  var platformArrowIndex = 1,
      bioArrowIndex = 1,
      platformItemCount = 5,
      bioItemCount = 3,
      arrowLeft = $('.arrow-left .arrow'),
      arrowRight = $('.arrow-right .arrow'),
      platformCurrent = '',
      bioCurrent = '';

  // Function to control action if left bio or platform button is clicked
  arrowLeft.click(function () {
    // Checks if this is for the Bio section
    if ($(this).parent().hasClass('bio-arrow')) {
      console.log('bio');
      bioArrowIndex -= 1;
      if (bioArrowIndex === 0) {
        bioArrowIndex = bioItemCount;
      }

      bioCurrent = 'bio-' + bioArrowIndex;

      $('.bio-topic').each(function() {
        $(this).hasClass(bioCurrent) ? $(this).removeClass('sr-only') : $(this).addClass('sr-only');
      });
    }
    // Or the Platforms section
    else {
      console.log('platforms');
      platformArrowIndex -= 1;
      if (platformArrowIndex === 0) {
        platformArrowIndex = platformItemCount;
      }

      platformCurrent = 'platform-' + platformArrowIndex;

      $('.platform-topic').each(function() {
        $(this).hasClass(platformCurrent) ? $(this).removeClass('sr-only') : $(this).addClass('sr-only');
      });
    }
  });

  // Function to control action if right bio or platform button is clicked
  arrowRight.click(function () {
    // Checks if this is for the Bio section
    if ($(this).parent().hasClass('bio-arrow')) {
      bioArrowIndex += 1;
      if (bioArrowIndex > bioItemCount) {
        bioArrowIndex = 1;
      }

      bioCurrent = 'bio-' + bioArrowIndex;

      $('.bio-topic').each(function() {
        $(this).hasClass(bioCurrent) ? $(this).removeClass('sr-only') : $(this).addClass('sr-only');
      });
    }
    // Or the Platforms section
    else {
      platformArrowIndex += 1;
      if (platformArrowIndex > platformItemCount) {
        platformArrowIndex = 1;
      }

      platformCurrent = 'platform-' + platformArrowIndex;

      $('.platform-topic').each(function() {
        $(this).hasClass(platformCurrent) ? $(this).removeClass('sr-only') : $(this).addClass('sr-only');
      });
    }
  });

});
